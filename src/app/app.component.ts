import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  data;
  apiUrl = "/test/token_ledger_report";
  payload = { "user_id": 27, "interval": "year" }
  dropdowndata = [{ label: 'LAST 30 DAYS', value: 'month' }, { label: 'LAST WEEK', value: 'week' }, { label: 'LAST YEAR', value: 'year' }]
  selectedDropdownValue = { label: 'LAST 30 DAYS', value: 'month' };
  constructor(private httpclient: HttpClient) {

  }

  ngOnInit() {
    this.loaddata(this.selectedDropdownValue);
  }

  loaddata(data){
    this.selectedDropdownValue = data;
    this.payload.interval = this.selectedDropdownValue.value;
    this.httpclient.post(this.apiUrl, this.payload).subscribe(resp => {
      this.data = resp['response'];
      this.data['transaction_records'].forEach(resp => {
        resp['transaction_date'] = resp['transaction_date'].replace(/\s/g, "T")
      })
    })
  }
}
